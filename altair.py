from peewee import *

database = MySQLDatabase('Altair', **{'user': 'oriol'})

class UnknownFieldType(object):
    pass

class BaseModel(Model):
    class Meta:
        database = database

class Alarm(BaseModel):
    alarm_description = TextField(null=True, db_column='Alarm description')
    date = DateField(null=True, db_column='Date')
    destination_ip = CharField(max_length=100, null=True, db_column='Destination IP')
    destination_user = CharField(max_length=100, null=True, db_column='Destination USER')
    id2 = PrimaryKeyField(db_column='ID2')
    organitation = CharField(max_length=100, db_column='Organitation')
    reference_number = CharField(max_length=100, null=True, db_column='Reference number')
    signatura_name = CharField(max_length=100, null=True, db_column='Signatura name')
    signature_description = TextField(null=True, db_column='Signature description')
    source_hostname = CharField(max_length=100, null=True, db_column='Source Hostname')
    source_ip = CharField(max_length=100, null=True, db_column='Source IP')
    time = TextField(null=True, db_column='Time')
    title = TextField(null=True, db_column='Title')

    class Meta:
        db_table = 'ALARM'

class Alerts(BaseModel):
    description = TextField(null=True, db_column='Description')
    impact = CharField(max_length=30, null=True, db_column='Impact')
    rn = CharField(max_length=30, primary_key=True, db_column='RN')
    reference = CharField(max_length=150, null=True, db_column='Reference')
    risk = CharField(max_length=100, null=True, db_column='Risk')
    rule = TextField(null=True, db_column='Rule')
    title = CharField(max_length=150, null=True, db_column='Title')
    type = CharField(max_length=30, null=True, db_column='Type')

    class Meta:
        db_table = 'Alerts'

class Attack_Exp(BaseModel):
    attack_exp = CharField(max_length=30, primary_key=True, db_column='Attack_exp')
    attack_exp_en = CharField(max_length=30, null=True, db_column='Attack_exp_en')
    exp = IntegerField(db_column='exp_ID')

    class Meta:
        db_table = 'Attack_exp'

class Attack_Req(BaseModel):
    attack_req = CharField(max_length=50, primary_key=True, db_column='Attack_req')
    attack_req_en = CharField(max_length=50, null=True, db_column='Attack_req_en')
    req = IntegerField(db_column='req_ID')

    class Meta:
        db_table = 'Attack_req'

class Bugtraq(BaseModel):
    bid = PrimaryKeyField(db_column='BID')
    description = CharField(max_length=100, null=True, db_column='Description')
    solution = TextField(null=True, db_column='Solution')
    timestamp = DateTimeField(db_column='Timestamp')
    altair = CharField(max_length=30, null=True, db_column='altair_ID')
    altair_estado = CharField(max_length=5, null=True)
    # FIXME: "class" is a reserved word, renamed.
    class_ = CharField(max_length=30, null=True, db_column='class')
    cve = CharField(max_length=20, null=True)
    local = CharField(max_length=20, null=True)
    object = CharField(max_length=20, null=True)
    published = CharField(max_length=20, null=True)
    remote = CharField(max_length=20, null=True)
    updated = CharField(max_length=20, null=True)
    vulnerable = CharField(max_length=250, null=True)

    class Meta:
        db_table = 'Bugtraq'

class Can(BaseModel):
    can = CharField(max_length=13, primary_key=True, db_column='CAN')
    comments = TextField(null=True, db_column='Comments')
    description = TextField(null=True, db_column='Description')
    phase = CharField(max_length=50, null=True, db_column='Phase')
    reference = TextField(null=True, db_column='Reference')
    votes = CharField(max_length=250, null=True, db_column='Votes')

    class Meta:
        db_table = 'CAN'

class Cert(BaseModel):
    code = CharField(max_length=25, primary_key=True, db_column='Code')
    timestamp = DateTimeField(db_column='Timestamp')
    title = CharField(max_length=250, null=True, db_column='Title')

    class Meta:
        db_table = 'CERT'

class Cve(BaseModel):
    cve = CharField(max_length=13, primary_key=True, db_column='CVE')
    description = TextField(null=True, db_column='Description')
    reference = TextField(null=True, db_column='Reference')

    class Meta:
        db_table = 'CVE'

class Causa(BaseModel):
    causa = CharField(max_length=50, primary_key=True, db_column='Causa')
    causa_en = CharField(max_length=50, null=True, db_column='Causa_en')

    class Meta:
        db_table = 'Causa'

class Confidence(BaseModel):
    confidence = CharField(max_length=50, primary_key=True, db_column='Confidence')
    confidence_en = CharField(max_length=50, null=True, db_column='Confidence_en')

    class Meta:
        db_table = 'Confidence'

class Estado(BaseModel):
    estado = CharField(max_length=30, primary_key=True, db_column='Estado')

    class Meta:
        db_table = 'Estado'

class Historico(BaseModel):
    comentario = CharField(max_length=255, db_column='Comentario')
    comentario_en = CharField(max_length=255, db_column='Comentario_en')
    fecha = DateField(db_column='Fecha')
    rn = CharField(max_length=50, primary_key=True, db_column='RN')
    version = CharField(max_length=10, db_column='Version')

    class Meta:
        db_table = 'Historico'

class Icat(BaseModel):
    ar_launch_locally = IntegerField(null=True, db_column='AR_Launch_locally')
    ar_launch_remotely = IntegerField(null=True, db_column='AR_Launch_remotely')
    ar_target_access_attacker = IntegerField(null=True, db_column='AR_Target_access_attacker')
    cve_description = TextField(null=True, db_column='CVE_Description')
    cve = CharField(max_length=13, primary_key=True, db_column='CVE_ID')
    ec_communication_protocol = IntegerField(null=True, db_column='EC_Communication_protocol')
    ec_encryption_module = IntegerField(null=True, db_column='EC_Encryption_module')
    ec_hardware = IntegerField(null=True, db_column='EC_Hardware')
    ec_network_protocol_stack = IntegerField(null=True, db_column='EC_Network_protocol_stack')
    ec_non_server_application = IntegerField(null=True, db_column='EC_Non_server_application')
    ec_operating_system = IntegerField(null=True, db_column='EC_Operating_system')
    ec_other = IntegerField(null=True, db_column='EC_Other')
    ec_server_application = IntegerField(null=True, db_column='EC_Server_application')
    ec_specific_component = TextField(null=True, db_column='EC_Specific_component')
    evaluator_comments = TextField(null=True, db_column='Evaluator_Comments')
    hl1 = TextField(null=True, db_column='HL1')
    hl2 = TextField(null=True, db_column='HL2')
    hl3 = TextField(null=True, db_column='HL3')
    hl4 = TextField(null=True, db_column='HL4')
    hl5 = TextField(null=True, db_column='HL5')
    lt_availability = IntegerField(null=True, db_column='LT_Availability')
    lt_confidentiality = IntegerField(null=True, db_column='LT_Confidentiality')
    lt_integrity = IntegerField(null=True, db_column='LT_Integrity')
    lt_obtain_all_priv = IntegerField(null=True, db_column='LT_Obtain_all_priv')
    lt_obtain_some_priv = IntegerField(null=True, db_column='LT_Obtain_some_priv')
    lt_sec_prot_other = IntegerField(null=True, db_column='LT_Sec_Prot_Other')
    lt_security_protection = IntegerField(null=True, db_column='LT_Security_protection')
    link1name = TextField(null=True, db_column='Link1Name')
    link1source = TextField(null=True, db_column='Link1Source')
    link1type = TextField(null=True, db_column='Link1Type')
    link2name = TextField(null=True, db_column='Link2Name')
    link2source = TextField(null=True, db_column='Link2Source')
    link2type = TextField(null=True, db_column='Link2Type')
    link3name = TextField(null=True, db_column='Link3Name')
    link3source = TextField(null=True, db_column='Link3Source')
    link3type = TextField(null=True, db_column='Link3Type')
    link4name = TextField(null=True, db_column='Link4Name')
    link4source = TextField(null=True, db_column='Link4Source')
    link4type = TextField(null=True, db_column='Link4Type')
    link5name = TextField(null=True, db_column='Link5Name')
    link5source = TextField(null=True, db_column='Link5Source')
    link5type = TextField(null=True, db_column='Link5Type')
    publish_date = DateTimeField(null=True, db_column='Publish_Date')
    references_a = TextField(null=True, db_column='References_a')
    severity = TextField(null=True, db_column='Severity')
    status_a = TextField(null=True, db_column='Status_a')
    vt_access_validation_error = IntegerField(null=True, db_column='VT_Access_validation_error')
    vt_boundary_condition_error = IntegerField(null=True, db_column='VT_Boundary_condition_error')
    vt_buffer_overflow = IntegerField(null=True, db_column='VT_Buffer_overflow')
    vt_configuration_error = IntegerField(null=True, db_column='VT_Configuration_error')
    vt_design_error = IntegerField(null=True, db_column='VT_Design_Error')
    vt_environment_error = IntegerField(null=True, db_column='VT_Environment_error')
    vt_exceptional_condition_error = IntegerField(null=True, db_column='VT_Exceptional_condition_error')
    vt_input_validation_error = IntegerField(null=True, db_column='VT_Input_validation_error')
    vt_other_vulnerability_type = IntegerField(null=True, db_column='VT_Other_vulnerability_type')
    vt_race_condition = IntegerField(null=True, db_column='VT_Race_condition')
    vuln_software = TextField(null=True, db_column='Vuln_Software')
    prev = TextField(null=True, db_column='prev_ID')

    class Meta:
        db_table = 'ICAT'

class Impacto(BaseModel):
    impacto = CharField(max_length=30, primary_key=True, db_column='Impacto')
    impacto_en = CharField(max_length=30, null=True, db_column='Impacto_en')
    rango = IntegerField(null=True, db_column='Rango')

    class Meta:
        db_table = 'Impacto'

class Microsoft(BaseModel):
    code = CharField(max_length=10, primary_key=True, db_column='Code')
    description = CharField(max_length=250, null=True, db_column='Description')
    timestamp = DateTimeField(db_column='Timestamp')

    class Meta:
        db_table = 'Microsoft'

class Nessus(BaseModel):
    bugtraq = CharField(max_length=10, null=True, db_column='Bugtraq_id')
    cve = CharField(max_length=15, null=True, db_column='CVE_id')
    description = TextField(null=True, db_column='Description')
    family = CharField(max_length=250, null=True, db_column='Family')
    plugin = CharField(max_length=10, primary_key=True, db_column='Plugin_id')
    timestamp = DateTimeField(db_column='Timestamp')
    title = CharField(max_length=250, null=True, db_column='Title')

    class Meta:
        db_table = 'Nessus'

class Osvdb(BaseModel):
    code = PrimaryKeyField(db_column='Code')
    description = TextField(null=True, db_column='Description')
    links = TextField(null=True, db_column='Links')
    risk = CharField(max_length=30, null=True, db_column='Risk')
    solution = TextField(null=True, db_column='Solution')
    timestamp = DateTimeField(db_column='Timestamp')
    title = CharField(max_length=50, null=True, db_column='Title')

    class Meta:
        db_table = 'OSVDB'

class Organitation(BaseModel):
    comments = TextField(null=True, db_column='Comments')
    contact = TextField(null=True, db_column='Contact')
    id = IntegerField(null=True, db_column='ID')
    organitation = TextField(null=True, db_column='Organitation')

    class Meta:
        db_table = 'Organitation'

class Plataformas(BaseModel):
    plat = IntegerField(db_column='Plat_id')
    plataformas = CharField(max_length=30, db_column='Plataformas')

    class Meta:
        db_table = 'Plataformas'

class Riesgo(BaseModel):
    riesgo = CharField(max_length=30, primary_key=True, db_column='Riesgo')
    riesgo_en = CharField(max_length=30, null=True, db_column='Riesgo_en')

    class Meta:
        db_table = 'Riesgo'

class Vulnera(BaseModel):
    attack_exp = CharField(max_length=30, null=True, db_column='Attack_exp')
    attack_req = CharField(max_length=50, null=True, db_column='Attack_req')
    bid = CharField(max_length=250, null=True, db_column='BID')
    cert = CharField(max_length=250, null=True, db_column='CERT')
    ciscobug = CharField(max_length=250, null=True, db_column='CISCOBUG')
    cve = TextField(null=True, db_column='CVE')
    causa = CharField(max_length=50, null=True, db_column='Causa')
    confidence = CharField(max_length=50, null=True, db_column='Confidence')
    descripcion = TextField(null=True, db_column='Descripcion')
    descripcion_en = TextField(null=True, db_column='Descripcion_en')
    estado = CharField(max_length=30, null=True, db_column='Estado')
    familia = CharField(max_length=50, null=True, db_column='Familia')
    fecha = DateField(null=True, db_column='Fecha')
    id = PrimaryKeyField(db_column='ID')
    impacto = CharField(max_length=30, null=True, db_column='Impacto')
    mskb = CharField(max_length=250, null=True, db_column='MSKB')
    plat_afectadas = TextField(null=True, db_column='Plat_afectadas')
    rn = CharField(max_length=50, null=True, db_column='RN')
    referencias = TextField(null=True, db_column='Referencias')
    revisar = CharField(max_length=2, null=True, db_column='Revisar')
    riesgo = CharField(max_length=30, null=True, db_column='Riesgo')
    solucion = TextField(null=True, db_column='Solucion')
    solucion_en = TextField(null=True, db_column='Solucion_en')
    timestamp = DateTimeField(db_column='Timestamp')
    titulo = CharField(max_length=200, null=True, db_column='Titulo')
    titulo_en = CharField(max_length=200, null=True, db_column='Titulo_en')
    version = CharField(max_length=10, null=True, db_column='Version')
    xf = CharField(max_length=250, null=True, db_column='XF')

    class Meta:
        db_table = 'Vulnera'

class Snort(BaseModel):
    # FIXME: "class" is a reserved word, renamed.
    class_ = CharField(max_length=150, null=True, db_column='Class')
    enable = CharField(max_length=2, null=True, db_column='Enable')
    family = CharField(max_length=50, null=True, db_column='Family')
    id = PrimaryKeyField(db_column='ID')
    rule = TextField(null=True, db_column='Rule')
    timestamp = DateTimeField(db_column='Timestamp')
    title = CharField(max_length=200, null=True, db_column='Title')

    class Meta:
        db_table = 'snort'

