import db
import xml.etree.ElementTree as et
from bs4 import BeautifulSoup

def getParser(name):
	return {
		'dnsenum': parseDNSENUM,
		'nmap': parseNMAP,
		'dnsrecon' : parseDNSRECON,
		'dmitry' : parseDMITRY,
		'theHarvester' : parseHARVESTER,
		'xprobe2' : parseXPROBE,
		'wafw00f' :parseWAFW00F,
		'nikto' : parseNIKTO,
		'metagoofil' : parseMETAGOOFIL,
		'wpscan' : parseWPSCAN
	}.get(name,None)


def parseDNSENUM(resource, audit):
	tree = et.parse(resource)
	root = tree.getroot()
	a = root[0]
	for host in a.findall('host'):
		name = host.find('hostname').text
		try:
			m = db.Machine.get((db.Machine.address == host.text) & (db.Machine.name == name) & (db.Machine.audit == audit))
			print 'machine already in DB'
		except db.Machine.DoesNotExist:
			m = db.Machine(address=host.text, name = name, audit = audit)
			m.save()	


def parseNMAP(resource, audit, machine=None):
	tree = et.parse(resource)
	root = tree.getroot()
	for host in root.findall('host'):
		if machine == None:
			address = host.find('address').get('addr')
			try:
				os = host.find('os')
				match = os.find('osmatch')
				name = match.get('name')
				cpe = match.find('osclass').find('cpe').text
			except Exception, e:
				print 'exception' , e
				name = 'unknown'
				cpe = 'unknown'		
			print cpe
			try:
				m = db.Machine.get((db.Machine.address == address)& (db.Machine.audit == audit))
				m.os = name
				m.cpe = cpe
				m.save()
				print 'update machine'
			except db.Machine.DoesNotExist:
				m = db.Machine(address=address, name = address, audit = audit, os=name, cpe=cpe)
				m.save()
		else:
			m = machine
		ports = host.find('ports')
		for port in ports.findall('port'):
			number = port.get('portid')
			service = port.find('service').get('name')
			state = port.find('state').get('state')
			try:
				product = port.find('service').get('product')
				cpe = port.find('cpe')
			except Exception, e:
				product = None
				cpe = None
			try:		
				p = db.Port.get((db.Port.machine == m) & (db.Port.number==number) & (db.Port.service == service) & (db.Port.state == state))
			except db.Port.DoesNotExist:
				p = db.Port(machine=m, number=number, service=service, state=state, product=product, cpe=cpe)
				p.save()



def parseDNSRECON(resource, audit):
	tree = et.parse(resource)
	root = tree.getroot()
	for record in root.findall('record'):
		rtype = record.get('type')
		if rtype != 'CNAME':
			addr = record.get('address')
			if rtype == 'NS':
				name = record.get('target')
			elif rtype == 'SOA':
				name = record.get('mname')
			elif rtype == 'MX':
				name = record.get('exchange')
			elif rtype == 'A':
				name = record.get('name')
			else:
				continue
			try:
				m = db.Machine.get((db.Machine.address == addr) & (db.Machine.name == name) & (db.Machine.audit==audit))
			except db.Machine.DoesNotExist:
				m = db.Machine(address=addr, name = name, audit = audit)
				m.save()


def parseDMITRY(resource, audit):
	f = open(resource).read()
	ini_sub = f.find('Gathered Subdomain')
	ini_email = f.find('Gathered E-Mail')
	subdomainString = f[ini_sub:ini_email]
	subdomain = {'name':'localhost','address':'127.0.0.1'}
	subdomains = subdomainString.split('\n')
	for line in subdomains:
		n = line.find('HostName:')
		a = line.find('HostIP')
		if n > -1:
			subdomain['name'] = line[n+9:]
		if a > -1: 
			subdomain['address'] = line[a+7:]
			try:
				m = db.Machine.get((db.Machine.address == subdomain['address']) & (db.Machine.name == subdomain['name']) & (db.Machine.audit==audit))
			except db.Machine.DoesNotExist:
				m = db.Machine(address=subdomain['address'], name = subdomain['name'], audit = audit)
				m.save()


def parseMETAGOOFIL(resource, audit):
	f = open(resource)
	soup = BeautifulSoup(f)
	for tag in soup.find_all('ul'):
		print tag.get('class')
		if tag.get('class')[0] == 'userslist':
			for item in tag.find_all('li'):
				name = item.text.encode('ascii','ignore')
				try:	
					e = db.Employee.get((db.Employee.name == name)&(db.Employee.audit == audit))
				except db.Employee.DoesNotExist:
					e = db.Employee(name=name,audit=audit)
					e.save()
					print e.name
				#print item.text
		if tag.get('class')[0] == 'emailslist':
			for item in tag.find_all('li'):
				mail = item.text.encode('ascii','ignore')
				try:	
					e = db.Employee.get((db.Employee.email == mail)&(db.Employee.audit == audit))
				except db.Employee.DoesNotExist:
					e = db.Employee(email=mail,audit=audit)
					e.save()
					print e.email
				#print item.text


def parseWAFW00F(resource, audit, machine=None):
	aux = resource.find('Checking')
	output = resource[aux:]
	tar_end = output.find('\n')
	target = output[9:tar_end]
	result = 'No WAF'
	lines = output.split('\n')
	for line in lines:
		if line.find('The site') >= 0:
			l = len(target) + 9
			result = line[l:]
	if machine != None:
		machine.waf = result
		machine.save()
	print result


def parseXPROBE(resource,audit, machine=None):
	tree = et.parse(resource)
	root = tree.getroot()
	target = root.find('target')
	addr = target.get('ip')
	osL = target.find('os_guess')
	os = osL.find('primary').text
	if machine != None:
		if machine.os == 'unknown':
			machine.os = os
			machine.save()
	else:
		try:
			machines = db.Machine.select().where((db.Machine.address == addr)&(db.Machine.audit==audit))
			for machine in machines:
				if machine.os == 'unknown':
					machine.os = os
					machine.save()
		except Exception, e:
			raise e

def parseHARVESTER(resource,audit):
	f = open(resource)
	soup = BeautifulSoup(f)
	for tag in soup.find_all('ul'):
		print tag.get('class')[0]
		if tag['class'][0] == "userslist":
			for elem in tag.find_all('li'):
				try:
					e = db.Employee.get((db.Employee.email== elem.string) &(db.Employee.audit==audit))
				except db.Employee.DoesNotExist:
					e = db.Employee(email = elem.string, audit = audit)
					e.save()
				print elem.string
		if tag['class'][0] == "pathslist":
			for elem in tag.find_all('li'):
				aux = elem.string
				aux = aux.split(':')
				address = aux[0]
				name = aux[1]
				try:
					m = db.Machine.get((db.Machine.address==address) & (db.Machine.name==name)&(db.Machine.audit==audit))
				except db.Machine.DoesNotExist:
					m = db.Machine(address=address, name=name, audit=audit)
					m.save()


def parseNIKTO(resource, port):
	tree = et.parse(resource)
	root = tree.getroot()
	scan = root.find('scandetails')
	for item in scan.findall('item'):
		osvdbid = item.get('osvdbid')
		description = item.find('description').text
		link = item.find('namelink').text
		try:
			v = db.Vulnerability.get((db.Vulnerability.osvdb==osvdbid) & (db.Vulnerability.port == port))
		except db.Vulnerability.DoesNotExist:
			v = db.Vulnerability(port=port, osvdb=osvdbid, link = link, description=description)
			v.save()

def parseWPSCAN(resource, port):
	f = open(resource,'r').read()
	ini_plugins = f.find('Enumerating installed plugins')
	ini_themes = f.find('Enumerating installed themes')
	ini_users = f.find('Enumerating usernames')
	plugins = f[ini_plugins:ini_themes]
	themes = f[ini_themes:ini_users]
	users = f[ini_users:]
	#plugins
	found = plugins.find('We found')
	if found > 0:
		plugins = plugins[found:]
		pluginList = plugins.split('[+]')
		pluginList.pop(0)
		for plugin in pluginList:
			line = plugin.find('\n')
			name = plugin[11:line]
			vuls = plugin.split('[!]')
			vuls.pop(0)
			#print name
			for vul in vuls:
				line = vul.find('\n')
				vulName = vul[12:line]
				osv = vul.find('osvdb')
				if osv > 0:
					aux = vul[osv-7:]
					end = aux.find('\n')
					osvdbid = aux[17:end]
					try:
						v = db.Vulnerability.get((db.Vulnerability.osvdb == osvdbid)&(db.Vulnerability.port == port))
					except Exception, e:
						v = db.Vulnerability(port = port, osvdb=osvdbid, description=vulName)
						v.save()