from flask import Flask
from flask import g
from flask import redirect
from flask import request
from flask import session
from flask import url_for, abort, render_template, flash
from functools import wraps
from hashlib import md5
import db
from peewee import SQL
import altair
import datetime
import monitor
#from monitor import escriure

app = Flask(__name__)
app.secret_key = 'some_secret'

def auth_user(user):
	session['logged_in'] = True
	session['userID'] = user.id
	session['username'] = user.username
	flash('You are logged in as %s' % (user.username))


def login_required(f):
	@wraps(f)
	def inner(*args, **kwargs):
		if not session.get('logged_in'):
			return redirect(url_for('login'))
		return f(*args, **kwargs)
	return inner

def object_list(template_name, qr, var_name='object_list', **kwargs):
    kwargs.update(
    	page=int(request.args.get('page', 1)),
    	pages=qr.count() / 20 + 1
    )
    kwargs[var_name] = qr.paginate(kwargs['page'])
    return render_template(template_name, **kwargs)

def get_object_or_404(model, **kwargs):

    try:
    	a = model.get(**kwargs)
    	return a
    except model.DoesNotExist:
    	abort(404)


@app.route("/")
def homepage(name = None):
	if session.get('logged_in'):
		return networks()
	else:
		return render_template('homepage.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST' and request.form['username']:
        try:
            user = db.User.get(
                username=request.form['username'],
                password=request.form['password']
            )
        except db.User.DoesNotExist:
            flash('The password entered is incorrect')
        else:
            auth_user(user)
            return redirect('/')

    return render_template('login.html')

@app.route('/register', methods=['GET', 'POST'])
def register():
	if request.method == 'POST' and request.form['username']:
		username = request.form['username']
		password = request.form['password']
		try:
			u = db.User.get(username=username)
			flash('Username already in use')
		except db.User.DoesNotExist:
			user = db.User(username=username, password=password, email = request.form['email'])
			user.save()
			auth_user(user)
			return redirect('/')
	return render_template('register.html')

@app.route('/logout')
def logout():
	session.pop('logged_in',None)
	flash('You were logged out')
	return redirect(url_for('login'))

@app.route('/createNetwork', methods=['GET', 'POST'])
@login_required
def createNetwork():
	if request.method == 'POST' and request.form['company']:
		try:
			net = db.Network.get(db.Network.company == request.form['company'])
			flash('Network already exists')
		except db.Network.DoesNotExist:
			user = db.User.get(db.User.id == session['userID'])
			net = db.Network.create(
				company=request.form['company'],
				createdBy=user
			)
			return redirect(url_for('networks'))
	return render_template('create_network.html')

@app.route("/networks")
@login_required
def networks():
	networks = db.Network.select()
	if networks.count() == 0:
		return redirect(url_for('createNetwork'))
	else:
		return object_list("network_list.html", networks, "network_list")

@app.route("/network/<int:network_id>")
@login_required
def network_detail(network_id):
	network = get_object_or_404(db.Network, id=network_id)
	return render_template("network_detail.html", network=network)

@app.route("/createAudit/<int:network_id>", methods=['GET','POST'])
@login_required
def create_audit(network_id):
	if request.method == 'POST' and request.form['target']:
		try:	
			net = get_object_or_404(db.Network, id=request.form['network'])
			user = db.User.get(db.User.id == session['userID'])
			aud = db.Audit.create(
				target=request.form['target'],
				created=datetime.datetime.now(),
				current_phase=None,
				progress=0,
				user = user,
				network = net
			)
			aud.save()
			return redirect(url_for('audit_detail',audit_id=aud.id))
		except Exception, e:
			raise e
	net = get_object_or_404(db.Network, id=network_id)		
	return render_template('create_audit.html', network=net)



@app.route("/audits/<int:network_id>")
@login_required
def audits(network_id):
	audits = db.Audit.select().where(db.Audit.network == network_id)
	return render_template('audit_list.html', audit_list=audits, network_id=network_id)
	#return object_list("audit_list.html", audits, "audit_list")

@app.route("/audit/<int:audit_id>", methods=['GET', 'POST'])
@login_required
def audit_detail(audit_id):
	if request.method == 'POST':
		try:
			x = request.form.getlist('phase')
			#needs to run in a terminal -> celery -A monitor worker --loglevel=info
			monitor.launch.delay(audit_id,x)	
			return redirect(url_for("audit_detail", audit_id=audit_id))
		except Exception, e:
			raise e
	audit = get_object_or_404(db.Audit, id=audit_id)
	wpscan = db.Vulnerability.select().join(db.Port).join(db.Machine).join(db.Audit).where((db.Audit.id == 4)&(db.Vulnerability.foundBy=='wpscan')).count()
	nikto = db.Vulnerability.select().join(db.Port).join(db.Machine).join(db.Audit).where((db.Audit.id == 4)&(db.Vulnerability.foundBy=='nikto')).count()
	print audit.target
	#audit = db.Audit.select().where(db.Audit.id == audit_id)
	return render_template("audit_detail.html", audit=audit, nikto=nikto, wpscan=wpscan)


@app.route("/machines/<int:audit_id>")
@login_required
def machines(audit_id):
	audit = get_object_or_404(db.Audit, id=audit_id)
	machines = db.Machine.select().join(db.Audit).where(db.Audit.id == audit_id)
	return render_template('machine_list.html', machine_list=machines, audit_id=audit_id)

@app.route("/employees/<int:audit_id>")
@login_required
def employees(audit_id):
	audit = get_object_or_404(db.Audit, id = audit_id)
	employees = db.Employee.select().join(db.Audit).where(db.Audit.id == audit_id)
	return render_template('employee_list.html', employee_list=employees, audit_id=audit_id)
'''
@app.route("/machine/<int:machine_id>")
@login_required
def machine_detail(machine_id):
	mach = get_object_or_404(db.Machine, id=machine_id)
	#mach = db.Machine.select().where(db.Machine.id == machine_id)
	ports = db.Port.select().join(db.Machine).where(db.Machine.id == machine_id)
	return render_template("machine_detail.html", machine=mach, port_list=ports)
	#return object_list("machine_detail.html", ports, "port_list", {"machine":mach})'''

@app.route("/port/<int:port_id>")
@login_required
def port_detail(port_id):
	port = get_object_or_404(db.Port, id=port_id)
	vuls = port.vulnerabilities
	for vul in vuls:
		try:
			aux = altair.Osvdb.get(altair.Osvdb.code==vul.osvdb)
			vul.os = aux
		except altair.Osvdb.DoesNotExist:
			pass
		except Exception, e:
			raise e
	return render_template("port_detail.html",port=port, vulnerabilities=vuls)

@app.route("/vul/<int:vul_id>")
@login_required
def vul_detail(vul_id):
	vul = get_object_or_404(altair.Osvdb, code = vul_id)
	return render_template('vul_detail.html', vul = vul)


@app.route("/audit/del/<int:audit_id>")
@login_required
def delete_audit(audit_id):
	a = db.Audit.get(db.Audit.id == audit_id)
	network_id = a.network.id
	a.delete_instance(recursive=True)
	return redirect(url_for('audits',network_id = network_id))


@app.route("/machine/vul/<path:cpe>")
@login_required
def cpe_vuls(cpe):
	if cpe != 'unknown':
		aux = "%"+cpe+"%"
		vuls = altair.Vulnera.select().where(altair.Vulnera.plat_afectadas % aux).order_by(SQL(" FIELD(Riesgo, 'Muy Alto', 'Alto','Medio','Bajo') "))
		ma = vuls.select().where(altair.Vulnera.riesgo == 'Muy Alto').count()
		a = vuls.select().where(altair.Vulnera.riesgo == 'Alto').count()
		m = vuls.select().where(altair.Vulnera.riesgo == 'Medio').count()
		b = vuls.select().where(altair.Vulnera.riesgo == 'Bajo').count()
		scores = {'ma':ma, 'a':a,'m':m,'b':b}
	else:
		vuls=None
		scores=None

	return render_template('altair_vuls.html', vuls = vuls, cpe=cpe, scores =scores)
	

	



if __name__ == '__main__':
	app.run(host='0.0.0.0')