import db, tool, parsers, os
import time
from celery import Celery

app = Celery('tasks', broker='amqp://guest@localhost//')

def run23(audit):
	q = db.Phase.select().where(db.Phase.order == 1)
	for p in q:
		q2 = db.Action.select().where(db.Action.phase==p)
		for a in q2:
			t = tool.Tool()
			t.name = a.tool
			t.options = a.options
			t.target = audit.target
			t.fileOutput = a.fileOutput
			parser = parsers.getParser(t.name)
			try:
				t.run()
				if t.fileOutput:
					parser('aux.xml', audit)
					os.remove('aux.xml')
				else:
					parser(t.getResult(), audit)
			except Exception, e:
				pass
			print t.name, 'done'


@app.task
def launch(audit_id, l):
	if "1" in l:
		runIG(audit_id)
	if "2" in l:
		runVA(audit_id)
	if "3" in l:
		runEX(audit_id)
	audit = db.Audit.get(db.Audit.id == audit_id)
	audit.current_phase = None
	audit.save()

def runVA(audit_id):
	audit = db.Audit.get(db.Audit.id == audit_id)
	q = db.Action.select().join(db.Phase).where(db.Phase.order == 2)
	p = db.Port.select().join(db.Machine).join(db.Audit).where((db.Audit.id == audit_id) & (db.Port.service == 'http'))
	audit.current_phase = 'Vulnerability Assessment'
	audit.progress = 0
	cont = 0
	audit.save()
	for port in p:
		for action in q:
			cont+=1
			addr = port.machine.address
			t = tool.Tool()
			t.name = action.tool
			t.options = action.options
			if action.options.find('%port%'):
				t.options = t.options.replace('%port%',str(port.number))
			t.setTarget(addr)
			t.fileOutput = action.fileOutput
			fileName = "%s.xml" % t.name
			if t.name == 'nikto':
				fileName = "%(name)s%(address)s%(port)s.xml" % {'name':t.name, 'address': addr, 'port': port.number}
				t.setFile(fileName)
			print t.options
			parser = parsers.getParser(t.name)
			try:
				t.run()
				if t.fileOutput:
					parser(fileName, port)
					os.remove(fileName)
				else:
					parser(t.getResult(), port)
			except Exception, e:
				pass
			print t.name, 'done'
			pro = float(cont) / p.count()
			audit.progress = pro
			audit.save()

	
	'''for a in q:
		run(a, audit)
		cont+=1
		p = float(cont) / q.count()
		audit.progress = p
		audit.save()'''


def runIG(audit_id):
	audit = db.Audit.get(db.Audit.id == audit_id)
	q = db.Action.select().join(db.Phase).where(db.Phase.order == 1)
	audit.current_phase = 'Information Gathering'
	audit.progress = 0
	total = q.count()
	cont = 0
	audit.save()
	for a in q:
		run(a, audit)
		cont+=1
		p = float(cont) / total
		audit.progress = p
		audit.save()
	print 'second step'
	machines = db.Machine.select().join(db.Audit).where(db.Audit.id == audit_id)
	total += machines.count()*3
	nmap = db.Action.get(db.Action.tool == 'nmap')
	xprobe = db.Action.get(db.Action.tool == 'xprobe2')
	waf = db.Action.get(db.Action.tool == 'wafw00f')
	unitools = [nmap,xprobe,waf]
	for m in machines:
		print m.address
		for action in unitools:
			t = tool.Tool()
			t.name = action.tool
			t.options = action.options
			t.target = m.address
			t.fileOutput = action.fileOutput
			parser = parsers.getParser(t.name)
			fileName = "%s.xml" % t.name
			try:
				t.run()
				if t.fileOutput:
					parser(fileName, audit, m)
					os.remove(fileName)
				else:
					parser(t.getResult(), audit, m)
			except Exception, e:
				pass
			cont+=1
			p = float(cont) / total
			audit.progress = p
			audit.save()
		print 'machine finished'


def runEX(audit_id):
	audit = db.Audit.get(db.Audit.id == audit_id)
	q = db.Action.get(db.Action.tool == 'msfconsole')
	audit.current_phase = 'Exploitation'
	audit.progress = 0
	cont = 0
	audit.save()
	run(q, audit)
	cont+=1
	p = 1
	audit.progress = p
	audit.save()


def run(action,audit):
	t = tool.Tool()
	t.name = action.tool
	t.options = action.options
	t.setTarget(audit.target)
	t.fileOutput = action.fileOutput
	parser = parsers.getParser(t.name)
	fileName = "%s.xml" % t.name
	try:
		t.run()
		if t.fileOutput:
			parser(fileName, audit)
			os.remove(fileName)
		else:
			parser(t.getResult(), audit)
	except Exception, e:
		pass
	print t.name, 'done'

