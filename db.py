import peewee
from peewee import *
db = MySQLDatabase('PTAS', user='oriol')

class BaseModel(peewee.Model):
	class Meta:
		database = db


class User(BaseModel):
	username = peewee.CharField()
	password = peewee.CharField()
	email = peewee.CharField()

class Network(BaseModel):
	company = peewee.CharField()
	createdBy = peewee.ForeignKeyField(User, related_name='networks')

class Audit(BaseModel):
	created = peewee.DateTimeField()
	target = peewee.CharField()
	current_phase = peewee.CharField(null=True)
	progress = peewee.FloatField(null=True)
	user = peewee.ForeignKeyField(User, related_name='audits')
	network = peewee.ForeignKeyField(Network, related_name='audits')


class Employee(BaseModel):
	name = peewee.CharField(null=True)
	email = peewee.CharField(null=True)
	audit = peewee.ForeignKeyField(Audit, related_name='employees')

class Machine(BaseModel):
	address = peewee.CharField()
	name = peewee.CharField()
	os = peewee.CharField(default='unknown')
	waf = peewee.CharField(default='unknown')
	cpe = peewee.CharField(default='unknown')
	audit = peewee.ForeignKeyField(Audit, related_name='machines')



class Port(BaseModel):
	machine = peewee.ForeignKeyField(Machine, related_name='ports')
	number = peewee.IntegerField()
	service = peewee.CharField()
	state = peewee.CharField()
	product = peewee.CharField()
	cpe = peewee.CharField()

class Vulnerability(BaseModel):
	port = peewee.ForeignKeyField(Port, related_name='vulnerabilities')
	name = peewee.CharField(null=True)
	osvdb = peewee.CharField(null=True)
	description = peewee.CharField(null=True)
	link = peewee.CharField(null=True)
	foundBy = peewee.CharField(default='nikto')



class Phase(BaseModel):
	name = peewee.CharField()
	order = peewee.IntegerField()
	#audit = peewee.ForeignKeyField(Audit, related_name='phases')


class Action(BaseModel):
	tool = peewee.CharField()
	options = peewee.CharField()
	active = peewee.BooleanField()
	fileOutput = peewee.BooleanField()
	phase = peewee.ForeignKeyField(Phase, related_name='actions')
	


def load_actions():
	try:
		ig = Phase.get(Phase.order == 1)
	except Phase.DoesNotExist:
		ig = Phase(name='Information Gathering', order=1)
		ig.save()
	try:
		va = Phase.get(Phase.order == 2)
	except Phase.DoesNotExist:
		va = Phase(name='Vulnerability Assessment', order=2)
		va.save()
	try:
		ex = Phase.get(Phase.order == 3)
	except Phase.DoesNotExist:
		ex = Phase(name='Exploitation', order=3)
		ex.save()
	load_file(ig,'IGactions.back')
	load_file(va, 'VAactions.back')
	load_file(ex, 'EXactions.back')

def load_file(phase, filename):
	f = open(filename).readlines()
	for line in f:
		l = line.split(',')
		try:
			a = Action.get(Action.tool == l[0])
		except Action.DoesNotExist:
			a = Action(tool = l[0],options=l[1], fileOutput=l[2], active=l[3], phase=phase)
			a.save()
		
def create_tables():
	User.create_table()
	Network.create_table()
	Audit.create_table()
	Employee.create_table()
	Machine.create_table()
	Port.create_table()
	Vulnerability.create_table()
	Phase.create_table()
	Action.create_table()


	





#usage of peewee classes

#Machine.create_table()
#m = Machine(address = '127.0.0.1', name = 'localhost')
#m.save()