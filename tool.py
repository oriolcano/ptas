import pexpect

class Tool:
	name = ""
	options = ""
	target = ""
	output = ""
	fileOutput = False

	def run(self, timeout=20000):
		if len(self.options) > 0:
			launch = "sudo "+self.name + " " + self.options + " " + self.target
		else:
			launch = "sudo "+self.name + " " + self.target
		log = open('log.txt','w')
		child = pexpect.spawn(launch, timeout=20000, maxread=20000, logfile = log)
		try:
			child.expect("password for oriol:",timeout = 5)
			child.sendline("oriol")
			child.expect(pexpect.EOF)
		except pexpect.EOF:
			pass			
		self.output = child.before

	def setOptions(self, op):
		self.options.append(op)

	def setTarget(self, tar):
		if self.options.find('%target%'):
			self.options = self.options.replace('%target%',tar)
		else:
			self.target = tar

	def setFile(self, f):
		if self.options.find('%file%'):
			self.options = self.options.replace('%file%',f)

	def getResult(self):
		return self.output