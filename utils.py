import re
#function to escape vt100 file formatting
def ansi_escape(text): 
	x = re.compile(r'\x1b[^m]*m')
	return x.sub('', text)